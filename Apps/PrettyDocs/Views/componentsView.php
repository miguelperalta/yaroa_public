<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><span aria-hidden="true" class="icon icon_puzzle_alt"></span> Components</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: June 07th, 2018</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">

                    <?php
                        echo Factory::getView("components-structure-module");
                        echo Factory::getView("components-structure-strmodule");
                        echo Factory::getView("components-structure-orm");
                    ?>

                </div><!--//content-inner-->
            </div><!--//doc-content-->
            <div class="doc-sidebar">
                <nav id="doc-nav">
                    <ul id="doc-menu" class="nav doc-menu hidden-xs" data-spy="affix">
                        <li><a class="scrollto" href="#module">Module</a></li>
                        <li>
                            <a class="scrollto" href="#module-components">Module Structure</a>
                            <ul class="nav doc-sub-menu">
                                <li><a class="scrollto" href="#controllers">Controllers</a></li>
                                <li><a class="scrollto" href="#models">Models</a></li>
                                <li><a class="scrollto" href="#views">Views</a></li>
                            </ul><!--//nav-->
                        </li>
                        <li><a class="scrollto" href="#orm">ORM</a></li>
                    </ul><!--//doc-menu-->
                </nav>
            </div><!--//doc-sidebar-->
        </div><!--//doc-body-->
    </div><!--//container-->
</div><!--//doc-wrapper-->