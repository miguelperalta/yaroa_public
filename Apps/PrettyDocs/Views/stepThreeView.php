<div id="gitinstaller"  class="section-block">
<h3 class="block-title">Via Git Installer</h3>
<p>
    First, download the YaroaFramework files using git:
</p>
<pre>
    <code class="language-git">
        $ git clone https://miguelperalta@bitbucket.org/miguelperalta/yaroa.git
    </code>
</pre>
</div><!--//section-block-->