
<section id="module-components" class="doc-section">

    <h2 class="section-title">Module Components</h2>

    <div id="controllers" class="section-block">
        <h3 class="block-title">Controller</h3>
        <p>
            The controller file receive all requests to module. They must extends from Abstract class 'AController'.
            <br><br>
            Here,
            <br><br>
            An example Controller
        </p>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <pre><code class="language-php">&lt;?php

namespace Defaults\Controllers;

//This file cannot be accessed from browser
defined('_EXEC_APP') or die('Ups! access not allowed');

use abstracts\Aorm;
use abstracts\Acontroller;

/**
 *
 * PHP version 5.4
 *
 * LICENSE: This source file is subject to the MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.
 *
 * @category   Controller
 * @package    Defaults\Controllers
 * @author     Miguel Peralta <mcalderon0329@gmail.com>
 * @license    https://opensource.org/licenses/MIT  MIT license
 * @since      File available since Release 2.1
 */

class AnnotationController extends Acontroller
{
    /**
     * AnnotationController constructor.
     * @param Aorm $model
     */
    public function __construct( Aorm $model ) {
        parent::__construct($model);
    }

    /**
     * @Routing[value=annotation,type=html]
    */
    public function annotation(){
        return "annotation";
    }

}

?&gt;</code></pre>
            </div>
        </div><!--//row-->
    </div><!--//section-block-->

    <div id="models" class="section-block">
        <h3 class="block-title">Model</h3>
        <p>
            If you need to get any information from database, you will need to create a Model file. This file must extend from <strong>'Aorm'</strong> class

        </p>
        <p>
            It's necessary to know how's the functionality of Object-Relation-Mapping (ORM) on Model class to execute action in database.
        </p>
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">

            <pre>
                <code class="language-php">&lt;?php

namespace Defaults\Models;

//This file cannot be accessed from browser
defined('_EXEC_APP') or die('Ups! access not allowed');

use abstracts\Aorm;
use stdClass;

/**
 * PHP version 5.4
 *
 * LICENSE: This source file is subject to the MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.
 *
 * @category   Model
 * @package    Defaults\Models
 * @Table[name=table_name]
 */

class AnnotationModel extends Aorm
{
    /**
     * @PrimaryKey
     * @AutoIncrement (optional)
     * @Column[name=field_name(optional),type=integer,alias=example(optional)]
     */
    private $id;

    /**
     * @var RelativeModel
     * @OneToMany[Entity=Module/Model,targetReference=key_name_in_this_class,target=target_key]
     */
    private $relative;

    /**
     * @var RelativeModel
     * @ManyToOne[Entity=Module/Model,targetReference=key_name_in_this_class,target=target_key]
     */
    private $relativeObject;

    /**
     * @var null
     */
    private $properties = null;

    /**
     * AnnotationModel constructor.
     * @param stdClass|null $properties object { server : ??, user : ??, pass : ??, db : ??, port : ??, provider: ??}
     */
    public function __construct( stdClass $properties = null ) {
        parent::__construct($this,$properties);
    }

    /**
     * @return null
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param null $properties
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
    }

}

?&gt;</code></pre>
            </div>


        </div><!--//row-->
        <br />
        <br />
        <p>
            Please check <strong>ORM</strong>
        </p>
    </div><!--//section-block-->

    <div id="views" class="section-block">
        <h3 class="block-title">View</h3>
        <p>
            HTML code must be here, this is a Yaroa example
        </p>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <pre><code class="code-block">
&lt;h1&gt; VIEW &lt;/h1&gt;

&lt;p&gt; Write your code!! &lt;/p&gt;
</code></pre>
            </div>
        </div><!--//row-->
    </div><!--//section-block-->

</section><!--//doc-section-->