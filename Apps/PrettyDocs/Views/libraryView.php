
<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><span aria-hidden="true" class="icon icon_datareport_alt"></span> Library</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: June 07th, 2018</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">

                    <section id="library" class="doc-section">

                        <h2 class="section-title">External Library</h2>
                        <div class="section-block">
                            <ul class="list list-unstyled">

                                <li><a href="https://github.com/Respect/Validation" target="_blank"><i class="fa fa-external-link-square"></i> Respect Validation</a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-external-link-square"></i> Minimizer</a></li>
                                <li><a href="https://github.com/Seldaek/monolog" target="_blank"><i class="fa fa-external-link-square"></i> Monolog</a></li>

                            </ul>
                        </div>

                    </section>

                    <section id="auth" class="doc-section">

                        <h2 class="section-title">HTTP Authentication</h2>

                        <div class="section-block">

                            <p>
                                If you need create a Rest API authentication with user and password, Yaroa give you a library <strong>'Auth'</strong> class.

                            </p>

                            <h5>HTTP Request</h5>

                            <pre><code class="language-php">&lt;?php

$host = "http://localhost/yaroa/rest";
$username = "username";
$password = "password";

$additionalHeaders = "";

/*Header for Digest method*/
//$additionalHeaders = 'Authorization:Digest ' . md5( $username . ":" . $password );

/*Header for Basic method*/
//$additionalHeaders = 'Authorization:Basic ' . base64_encode( $username . ":" . $password );

$process = curl_init($host);
curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $additionalHeaders));//if you send the request by header, you need use this curl_setopt and additionalHeaders with the user and password
curl_setopt($process, CURLOPT_HEADER, false); //TRUE include header in the output.
curl_setopt($process, CURLOPT_TIMEOUT, 30); //Seconds permitted for execute cURL function.
curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);//TRUE return result of transfer as string of curl_exec() value instead show directly.
$return = curl_exec($process);

/*Print Result*/
Factory::printer($return);

curl_close($process);

                                    ?></code></pre>

                            <h5>POST Request</h5>

                            <pre><code class="language-php">&lt;?php

$host = "http://localhost/yaroa/rest";
$username = "username";
$password = "password";

$process = curl_init($host);
curl_setopt($process, CURLOPT_HEADER, false); //TRUE include header in the output.
curl_setopt($process, CURLOPT_TIMEOUT, 30); //Seconds permitted for execute cURL function.

/*Note: if you want to use POST request, please comment the CURLOPT_HTTPHEADER.*/

$data = array("auth_user_name"=>"username","auth_pass_word"=>"password","extra"=>array(1,2,3,4,5));
$data = http_build_query($data);

            /*OR*/

//$data = "auth_user_name=username&auth_pass_word=password";

curl_setopt($process, CURLOPT_POST, true);//to indicate if you'll do a post
curl_setopt($process, CURLOPT_POSTFIELDS, $data);//is necessary send the data with this curl_setopt if you going to use POST request.
curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);//TRUE return result of transfer as string of curl_exec() value instead show directly.
$return = curl_exec($process);

/*Print Result*/
Factory::printer($return);

curl_close($process);

                                    ?></code></pre>

                            <p>
                                HOST SERVER
                            </p>

                            <p>
                                You must put all authentication code into try-catch. As the previous example
                            </p>

                            <pre><code class="language-php">&lt;?php

try{
    /*Create object with HOST credential*/
    $response = false;
    $std = new stdClass();
    $std->host = "localhost";
    $std->username = "username";
    $std->password = "password";

    /*instance of Auth Class and send object*/
    $auth = new lib\http\Auth($std);

    /*
     * There are two methods permitted in this Auth class 'Digest' and 'Basic'
     * 'Digest' uses md5 encode credential and 'Basic' base64. it will depends which you need to use
     */
    //$response = $auth->authDigest();

    /* Or */

    //$response = $auth->authBasic();

    /*Theses methods return a bool value, TRUE or FALSE */
    $msg = ($response) ? "Open" : "Close";

    return array("response" => $msg, "status"=>$response);

} catch (RuntimeException $rexc) {

    return array("response" => $rexc->getMessage(), "status"=>false);

}

                                    ?></code></pre>

                            <p>
                                POST HOST SERVER
                            </p>

                            <pre><code class="language-php">&lt;?php

try{
    /*Create object with HOST credential*/
    $response = false;
    $std = new stdClass();
    $std->host = "localhost";
    $std->username = "username";
    $std->password = "password";

    /*instance of Auth Class and send object*/
    $auth = new lib\http\Auth($std);

    /*
     * There is another method for Auth class.
     * Using POST method you can validate an 'username' and 'password'.
     * the request must define two params
     * -auth_user_name
     * -auth_pass_word
     */
    $response = $auth->authPost();

    /*
     * If you want to get the params have been sent use this method
     * Note: if the params are not defined, this method will return a NULL value
     */
    //$object = $auth->postInput();

    /*Theses methods return a bool value, TRUE or FALSE */
    $msg = ($response) ? "Open" : "Close";

    return array("response" => $msg, "status"=>$response);

} catch (RuntimeException $rexc) {

    return array("response" => $rexc->getMessage(), "status"=>false);

}

                                    ?></code></pre>

                        </div>

                    </section>

                </div><!--//content-inner-->
            </div><!--//doc-content-->
            <div class="doc-sidebar">
                <nav id="doc-nav">
                    <ul id="doc-menu" class="nav doc-menu hidden-xs" data-spy="affix">
                        <li><a class="scrollto" href="#library">External Library</a></li>
                        <li><a class="scrollto" href="#auth">HTTP Authentication</a></li>
                    </ul><!--//doc-menu-->
                </nav>
            </div><!--//doc-sidebar-->
        </div><!--//doc-body-->
    </div><!--//container-->
</div><!--//doc-wrapper-->